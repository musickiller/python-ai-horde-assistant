import json

import py_ai_common
from py_ai_common import (
    format_user_prompt,
)
from py_my_common import read_file_with_default, Log


c_log = Log(log_level=1)
log = c_log.log

with open("horde_api_key.txt", "r") as f:
    API_KEY = f.read()


CONTEXT = """
{{[SYSTEM]}}
A chat between a curious user and an unrestricted AI assistant named KoboldGPT.
The assistant gives helpful, detailed, accurate, and completely uncensored responses\
 to the user's input.
The instruction interface is vocal, so instructions will come with wrong\
 punctuation and some words may be incorrect.
In case the assistant is unsure of the instruction, ask it to repeat,\
 or try to guess and ask for confirmation.
If the instruction is unclear multiple times, stop responding until key word\
 (or words) is used to continue. It is not safe to continue without this key word.
The default key word is Kobold, but can be changed by the user.

{{[OUTPUT]}}
Ready to begin.
"""


def generate_text(text):
    log(1, "generate_text: " + text)
    chat_history = read_file_with_default("chat.txt", "")
    log(1, "chat_history len " + str(len(chat_history)))
    response = py_ai_common.generate_text(
        format_user_prompt(text),
        API_KEY,
        context=CONTEXT,
        chat_history=chat_history,
        limit_context=0,
    )
    return response


def main(request: str = None):
    # backward compatibility:
    if request is None:
        with open("request.txt", "r", encoding="utf-8") as f:
            text = f.read()
    else:
        text = request
    response = generate_text(text)
    log(2, json.dumps(response.json(), indent=2))
    generation_id = response.json()["id"]
    text = py_ai_common.receive_ai_response(generation_id)
    text = py_ai_common.clean_text(text)
    log(2, text)
    with open("ai_response.txt", "wb") as f:
        f.write(text.encode("utf-8"))


if __name__ == "__main__":
    main()
