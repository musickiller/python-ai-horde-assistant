from py_my_common import read_file_with_default


CHAT_HISTORY_FILE_PATH = "history.txt"

def main(request: str = None):
    chat = read_file_with_default(CHAT_HISTORY_FILE_PATH, "")

    if not request:
        request = read_file_with_default("request.txt", "")

    with open("ai_response.txt", "r", encoding="utf-8") as f:
        response = f.read()

    chat = (
        chat
        + "\n{{[INPUT]}}\n"
        + request
        + "\n\n{{[OUTPUT]}}\n"
        + response
        + "\n"
    )
    with open(CHAT_HISTORY_FILE_PATH, "w", encoding="utf-8") as f:
        f.write(chat)


if __name__ == "__main__":
    main()
