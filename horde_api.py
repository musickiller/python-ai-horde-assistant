from typing import TypedDict

import requests

BASE_URL = "https://stablehorde.net/api/"


def status(id):
    URL = f"v2/generate/text/status/{id}"
    response = requests.get(
        BASE_URL + URL,
        headers={
            "accept": "application/json",
            "Client-Agent": "unknown:0:unknown",
        },
    )
    return response


def delete(id):
    URL = f"v2/generate/text/status/{id}"
    response = requests.delete(
        BASE_URL + URL,
    )
    return response


ModelStatus = TypedDict(
    "ModelStatus",
    {
        "performance": float,
        "queued": int,
        "jobs": int,
        "eta": int,
        "type": str,
        "name": str,
        "count": int,
    },
)


def models(type="text", state="all") -> list[ModelStatus]:
    URL = f"v2/status/models?type={type}&model_state={state}"

    return requests.get(
        BASE_URL + URL,
    ).json()
