import argparse
import winsound

from ai import main as ai
from collect import main as collect

from record import record
from stt import main as stt
from transcribe import main as transcribe

parser = argparse.ArgumentParser(
    prog="Python Horde AI Assistant",
    description="some description",
    epilog="some epilog",
)
parser.add_argument("-s", "--silent", action="store_true")

VOCAL = not parser.parse_args().silent

while True:
    winsound.Beep(300, 500)
    if VOCAL:
        record()
        winsound.Beep(440, 500)
        result = transcribe()
        winsound.Beep(500, 500)
    else:
        command = input("Request: ")
        with open("request.txt", "w", encoding="utf-8") as f:
            f.write(command)
    ai(result)
    if VOCAL:
        stt()
    collect(result)
