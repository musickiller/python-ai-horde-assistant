import pyaudio
import audioop
import time
import wave

# Constants
FORMAT = pyaudio.paInt16
CHANNELS = 1
RATE = 44100
CHUNK = 1024
SILENCE_THRESHOLD = 1000  # Define silence threshold
CLAP_THRESHOLD = 8000  # Define clap threshold
SILENCE_DURATION = 3  # Duration in seconds
RECORD_SECONDS = 10  # Maximum recording time
WAVE_OUTPUT_FILENAME = "./rec.wav"


def record():
    audio = pyaudio.PyAudio()

    # Start audio stream
    stream = audio.open(
        format=FORMAT, channels=CHANNELS, rate=RATE, input=True, frames_per_buffer=CHUNK
    )

    print("Waiting for the loud sound...")

    frames = []  # A list to store audio chunks
    do_record = False

    try:
        while True:
            data = stream.read(CHUNK, exception_on_overflow=False)
            rms = audioop.rms(data, 2)  # Get RMS of the chunk

            if not do_record:
                if rms < CLAP_THRESHOLD:
                    continue

                print("Sound detected, starting recording.")
                do_record = True
                recording_start = time.time()
                silence_start = None

            frames.append(data)  # Add the chunk to the list of frames

            if rms < SILENCE_THRESHOLD:
                if silence_start is None:
                    # Just entered silence
                    silence_start = time.time()
                else:
                    # Check how long we've been silent
                    if time.time() - silence_start > SILENCE_DURATION:
                        print("Silence detected for 3 seconds, stopping recording.")
                        break
            else:
                # Reset silence start as there is sound
                silence_start = None

            # Stop recording after RECORD_SECONDS
            if time.time() - recording_start > RECORD_SECONDS:
                print("Maximum recording time reached, stopping recording.")
                break

    except KeyboardInterrupt:
        pass

    finally:
        # Stop and close the stream
        stream.stop_stream()
        stream.close()
        audio.terminate()

        # Save the recorded data as a WAV file
        wf = wave.open(WAVE_OUTPUT_FILENAME, "wb")
        wf.setnchannels(CHANNELS)
        wf.setsampwidth(audio.get_sample_size(FORMAT))
        wf.setframerate(RATE)
        wf.writeframes(b"".join(frames))
        wf.close()

        print(f"Recording stopped. Audio saved to '{WAVE_OUTPUT_FILENAME}'.")
