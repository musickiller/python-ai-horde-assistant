import pyttsx3


def main():
    engine = pyttsx3.init()
    with open("ai_response.txt", "r", encoding="utf-8") as f:
        text = f.read()
    engine.say(text)
    engine.runAndWait()
