import subprocess
from time import sleep

from py_my_common import Log


c_log = Log(log_level=2)
log = c_log.log


APP_PATH = "/tmp/transcribot"


def main():
    # result = subprocess.check_output(
    #     ["ssh", "dima@10.42.0.1", "echo meow"],
    #     # shell=True,
    #     text=True,
    #     # stderr=subprocess.STDOUT,
    # )
    # result = subprocess.check_output("where.exe ssh")

    send()
    while not check():
        sleep(1)

    result = get_result()
    delete()
    return result


def send():
    log(1, "send")
    return subprocess.call(
        ["scp", "rec.wav", f"dima@10.42.0.1:{APP_PATH}/rec.wav"],
        # shell=True,
        text=True,
        # stderr=subprocess.STDOUT,
    )


def check():
    log(1, "check")
    result = subprocess.call(
        ["ssh", "dima@10.42.0.1", "ls", f"{APP_PATH}/rec.txt"],
    )
    if result == 0:
        return True
    if result == 2:
        return False
    raise Exception("Unexpected error code:", result)


def get_result():
    log(1, "get_result")
    result = subprocess.check_output(
        ["ssh", "dima@10.42.0.1", "cat", f"{APP_PATH}/rec.txt"],
        text=True,
        encoding="utf-8",
    )
    log(2, result)
    return result


def delete():
    log(1, "delete")
    subprocess.check_call(
        ["ssh", "dima@10.42.0.1", "rm", f"{APP_PATH}/rec.txt"],
        text=True,
    )


if __name__ == "__main__":
    main()
